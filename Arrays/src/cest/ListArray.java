package cest;

import java.util.ArrayList;

public class ListArray {

	public static void main(String[] args) {
		
		ArrayList truf = new ArrayList();
		truf.add("Mong");
		truf.add("Krobos");
		truf.add("Traco");
		
		
		ArrayList bog = new ArrayList();
		bog.add("Demos");
		bog.add("Virgu");
		bog.add("Hitk");
		
		System.out.println("quantidade de elementos de truf: " + truf.size());
		System.out.println("quantidade de elementos de bog:" + bog.size());
		
		truf.remove(0);
		bog.remove(2);
		
		System.out.println("quantidade de elementos de truf: " + truf.size());
		System.out.println("quantidade de elementos de bog: " + bog.size());
	}

}

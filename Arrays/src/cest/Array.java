package cest;

import java.util.Arrays;

public class Array {

	public static void main(String[] args) {
		
		String [] corm = {"tatu", "tatu", "viado", "jabuti", "palhaço"};
		String [] dorft = {"tatu", "tatu", "viado", "jabuti", "palhaço"};
		String [] usk = {"altolfo", "baku", "tif", "flivosk", "azof"};
		
		if(Arrays.equals(corm, dorft)) {
			System.out.println("corm é iguail a dorft");
		}
		else {
			System.out.println("corm é diferente de dorft");
		}
		
		if(Arrays.equals(corm, usk)) {
			System.out.println("corm é iguail a usk");
		}
		else {
			System.out.println("corm é diferente de usk");
		}
	}

}
